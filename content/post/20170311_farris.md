---
title: "The Books of Farris"
description: "Wherein the team investigates the circumstances of Timothy Farris and the whereabouts of his nuyen."
author: "James B."
date: "2017-03-11"
tags:
    - "gangs"
    - "datasteal"
    - "recon"
---
"My client believes that a business associate of his is not keeping on the up-and-up. We need you to figure out what he's up to."

Mr. Johnson was an olive-skinned, dark-haired dwarf wearing a nondescript black suit. He sat across from the team at a booth in the Tidbit, whose proprietress, Tidbit, set up the meet. The troll and human that comprised Mr. Johnson's muscle occupied themselves with a game of Go Fish at a nearby table. Neither player was winning. The job Mr. Johnson offered was simple enough: locate Timothy Farris' financial records&mdash;his *real* financial records&mdash;and find out where the money went. The team agreed to a rate of &yen;5000 each, then left to investigate Timothy's home.

Part One: Farris' Apartment
---------------------------
At Timothy's home address in Tacoma, the team found to a brick apartment building that was in excellent repair for its age. The sun had begun its descent, and the streets were empty, save for a few small pockets of metahumans. Growler and EddY noticed a group of metahumans clad in purple and gold, who Growler identified as being members of the **257 Star Killers**. He noted that the gang was new, and had between 20 and 30 members. Since the gang members were passive, the team opted to ignore them. They instead proceeded into the apartment building and toward Timothy's apartment.

It didn't take long to find Timothy's apartment, and with a little help from one of EddY's spirits, they managed to bypass the lock. The inside of the apartment was pretty spartan, but there was a terminal, which Growler's hacked with little difficulty. On the terminal, Growler found a cache of emails, with a number of names being of particular interest. Those names were:

* Sara Farris
* Jonathon Farris
* Terry Farris
* Gino Acconci
* Daido Hanamura
* Michelle Pratt

With some additional searching, the runners concluded that they had found everything they could in the apartment, and decided to leave. On the way out, EddY opted to approach a group of three 257 Star Killers to see if they knew anything of Timothy or his whereabouts. The conversation quickly turned sour as the gang-bangers were not interested in talking, or in EddY's peace offering of an origami flower. Having grown impatient, Riley opted to enact Plan A: Violence. The altercation went largely as expected, with the gang-bangers proving no match for a team of skilled runners.

With three fallen gangers at their feet, and sirens rapidly drawing near, the group had to think quickly. With little time to spare, Growler popped open a manhole cover and the group dumped the bodies inside. With the gangers disposed of, the team rushed to their vehicles and scattered to the winds.

Part Two: Fleeing the Scene
---------------------------
Riley and EddY decided the best course of action was to lay low at a chop shop owned by Miss Grievous, one of Riley's contacts. Miss Grievous was a heavily augmented troll in a business suit who gave off a cool, professional air. She did not relish the thought of two runners bringing heat to her place of business, but did agree to allow the runners stay so long as they kept out of site, on the condition that Riley repay the debt at a later date. Grievous then left the pair to their devices, and riley pulled her van to the back of the shop.

It didn't take long for EddY to become restless, and he quickly slipped out of the van and set to work aligning the chi around the garage. Satisfied, he climbed onto the roof of the garage and launched his kite into the air. One effortless application of magic later and he had a clear view of the area. He searched for any sign of activity that might suggest that he and Riley were followed, yet all he could find was a few scattered incidents of unrelated gang activity. This was enough to spook Miss Grievous, and she quickly had the two pack up and leave, but not before adding on to Riley's debt.

Meanwhile, Growler had returned to The Tidbit to comfort himself with an order of Hawaiian ribs. That comfort didn't last for very long, however, when Mr. Johnson called to express concern over some incidents that had happened near Farris' apartment. He curtly explained the importance of the job, and of being discreet, then hung up before Growler could get a word in edgewise. That didn't stop Growler from trying, however. Growler then reported to conversation to Riley, finished his ribs, then set off to collect EddY, who Riley had dumped in a parking lot out of frustration.

Part Three: The Obligatory Warehouse
------------------------------------
With EddY out of the way, Riley set off to stake out Farris' warehouse. She found a ruined building to set up in, then began surveying the perimeter, where she found a number of people rummaging through some containers outside of the building. One of the figures was a dark-haired man in a grey suit, while another was revealed to be a Star Killer. Riley also spotted a figure lurking in the shadows beneath a truck trailer, but couldn't make out what it was. She deigned to watch over the scene until Growler and Eddy arrived on site.

At Growler's apartment, EddY entertained himself by improving the Feng Shui in the room. When Growler rejoined EddY, his home had a fresh spring scent, but he couldn't figure out why. The two bonded over beer and cupcakes with pink frosting, then left for work.

Growler and EddY parked their bike a couple of blocks away from the warehouse, then approached the warehouse. Growler effortlessly bypassed the maglock that feebly attempted to prevent their entry, and the pair were greeted by the Star Killers' door man. He was a corpulent human sporting a purple and gold nylon jacket who held a bearclaw in one hand, and a shotgun in the other. Growler failed to convince the man to let him past, but EddY had him eating out of his hand. In no time, the two runners were on their way up the stairs toward Timothy's office.

When the pair stepped into Timothy's office, they were confident that they were on the home stretch. Then it hit them; the sight of Timothy's body slumped over his desk, with a bloody gash on the back of his head; the rhythmic plip-plop of blood dripping into a pool on the tile floors; the stench of blood; and the gore-soaked paperweight on the desk next to him. Timothy had been murdered, presumably by the gangers ransacking the warehouse. This made the cooperation of the gang-banger in the lobby all the more suspicious.

Part Four: Get the Data and Get Out
-----------------------------------
Realizing that it wouldn't be long until the Gangers tossing the warehouse figured out what was going on, EddY and Growler had to act fast. Growler got to work collecting the necessary data. He located, then took, Farris' datapad and began tapping furiously at the keyboard on Farris' computer. Meanwhile, EddY searched for an exit. What he found was a window facing toward an alleyway, so he got to work setting up his grapping hook. He let the rope fall down to the ground, and ensured that the hook was securely attached.

Outside of the warehouse, Riley noticed the man in the grey suit had grown agitated. The man pulled open the door and ushered one of the gangers, along with a dog that had been hiding under a trailer, inside before following them. Riley gave EddY and Growler five minutes to do what they had to do and get out, then started tearing down her sniper's nest.

Back inside, Growler was making short work of the security on the Terminal. A progress bar on the terminal screen hit the 100% mark, and the job was done. All that was left was for the two runners to escape. In a last ditch effort to deter their pursuers, Growler placed a claymore mine underneath Farris' office chair before joining EddY at the window. EddY explained how to climb down the rope, then began his descent. He failed to heed his own instructions and lost his grip, tumbling down to the ground below. Growler went down after EddY just as the door to the office flew open, and soon the pair were laying in a crumpled heap in the alley.

By this time, three and a half minutes had passed, and Riley was impatient. She slung her rifle over her shoulder and returned to her van just in time to hear the explosion coming from the warehouse. Cursing the morons she'd been stuck with, she angrily sped away from the scene.

Meanwhile, Growler and EddY sprinted up the street toward their motorcycle. The warehouse behind them shook with the force of the explosion, and gangers rushed out from the receiving area to open fire on the pair. Growler took a couple of minor flesh wounds, but the two runners avoided the brunt of the assault. They climbed onto the motorcycle and beat a hasty retreat. Growler then called Riley, giving her instructions to meet up at the Tidbit.

Part Five: Wrapping Up
----------------------
After a close call with Lone Star for Growler, and a close shave for EddY, the pair was enjoying a Hawaiian pizza at the Tidbit. Riley stormed in shortly after, and once she had vented her anger at her two teammates, she stormed off to the bar to drown her frustration with booze. Once Riley had left, Growler called Mr. Johnson and set up an exchange, though he was careful to avoid mentioning what had happened to Farris.

It wasn't long before a tired Mr. Johnson ambled into the Tidbit, with his troll muscle and a young elf woman in tow. The runners handed over the datapad, the commlink, and the data, then explained the situation. Johnson was incredulous that the 257 Star Killers could have been behind the whole affair, given their status and lack of manpower. Meanwhile, the elf, who had been jacked into her deck up to this point, signaled to Mr. Johnson that the data was good, and the runners were paid for their work.

The Johnson then revealed that there was product in the warehouse that belonged to his client, and that the runners would have to retrieve it. With that, negotiation began for the next job.
