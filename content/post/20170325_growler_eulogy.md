---
title: "Growler's Eulogy"
description: "A few words in memory of our fallen comrade."
author: "James B."
date: "2017-03-25"
tags:
    - "death"
    - "growler"
    - "eulogy"
---
<center><img src="//sranarchy.jamesbishop.ca/img/runners/growler.jpg" alt="Growler" style="border-radius:50%;"></center>

Growler was a good decker, and a good man. He was&mdash;He was one of us. He was an ork who loved the outdoors, and cupcakes, and as a foodie explored the eateries of New Seattle from Everett to Auburn, and up to Bellevue. And he was a lover of cupcakes. And a good friend. He died&mdash;he died as so many of his generation, before his time. In your wisdom you took him, Lord, as you took so many bright, flowering young men, at Tacoma and Puyallup AND BUILDING 92! These young men gave their lives. And Growler too. Growler...who...who loved cupcakes. And so, Growler&mdash;Brog Iddultane&mdash;in accordance with what we think your dying wishes might well have been, we commit your final mortal remains to the bosom of the Pacific Ocean, which you loved so well.

Goodnight, sweet prince.

*Adapted from the <a href="https://www.youtube.com/watch?v=_4ezPvzKe5M" target="_blank" rel="noopener noreferrer">Donny's Ashes scene</a> in The Big Lebowski.*
