---
title: "Death at the Warehouse"
description: "Wherein the team attempts to locate Mr. Johnson's shipment."
author: "James B."
date: "2017-04-02"
tags:
    - "gangs"
    - "lone star"
    - "death"
    - "retrieval"
---
After a series of whirlwind negotiations, the team had it's second job. Mr. Johnson had the data his client requested, but with Timothy Farris dead, the group would also have to track down some cargo that Farris was holding on to. The group would receive &yen;6000 each for their efforts, and Mr. Johnson would help to furnish the team with the gear they'd need to complete the job. Mr. Johnson then supplied a shipping manifest with the relevant items highlighted, and the team was sent on its way.

Part One: The Team Returns to the Warehouse
-------------------------------------------
The team wasted no time in getting to work, and, still drunk from their time at the Tidbit, they pulled up to the warehouse. The difference was that the warehouse was now crawling with Lone Star, who were investigating the altercation that took place only a few hours before. The team discussed their plan, then EddY got out of the van and approached the nearest Lone Star officer, claiming to be an officer and looking to gain entry. This did not go well.

Things quickly went downhill. EddY intimidated one of the Lone Star officers on site, and thinking that he was being led to his objective, was instead led into a trap and was quickly detained. Riley drunkenly stumbled out of the van and attacked the officer that EddY originally spoke to. Growler, panicked by the number of Lone Star officers present, turned on his signal jammer, disabling Riley's drone. The net result was one imprisoned mage, one useless drone, and one dead officer. To make matters worse, a van full of Star Killers showed up and began shooting up the Lone Star vehicles.

One of the Star Killers, the one that EddY had duped previously, recognized Growler, and broke from the group to confront him. Meanwhile, the remaining gangers were approached by an elf who introduced himself as Raven *freaking* Mallory, and began questioning them about his contact. Posturing ensued, but a few well placed shots and one dead ganger were enough to send the rest running. The portly ork that had engaged Growler and Riley also tried to flee, but was unable to reach the van before being slain by Riley.

Once things had settled down outside, Riley and Growler entered the warehouse through the receiving door, while Raven went in through the office entrance.

Part Two: Fleeing the Scene
----------------------------
After introducing themselves and agreeing to assist each other with their respective problems, the trio began their search for the items specified on the manifest. They were unable to find anything before they heard the familiar wail of sirens, however. Thinking quickly, they resolved to steal two delivery trucks filled with goods while Riley led Lone Star away from the scene. In spite of her state of inebriation, Riley succeeded in outrunning Lone Star with minimal collateral damage, while Growler and Raven brought the trucks back to the Tidbit.

Once the team had regrouped at the Tidbit, they searched the contents of the delivery trucks that they had stolen, looking for any sign of the goods Mr. Johnson had requested. Two of the packages were found on board, and Growler was happy to call it a day because hey, two out of three isn't that bad. Riley wasn't satisfied, however, and after a couple rounds of friendly persuasion were fired, the team was once again en route to the warehouse.

Part Three: The Team Returns to the Warehouse
---------------------------------------------
This time, when the team returned they found two armored vans, and eight heavily armored Lone Star officers guarding the warehouse. Riley was unfazed, however, and fired a shot at the gas tank on one of the vans, causing it to go up in flames. This put the officers on high alert, and they quickly pinpointed Riley's location and moved to neutralize her. Riley responded by rushing up to the roof of the building she had set up in, hoping that she would be able to jump to an adjacent building and lose the officers on her tail.

On the ground, Growler and Raven did their best to pass as bystanders as Growler used his signal jammer to block communication in the area. Raven then decided that it was a good time to try convincing one of the officers, who up to that point had been rushing to respond to the sniper fire, to let him and Growler into the warehouse. After a tense exchange between the two, Raven and Growler were left zip tied to the ground.

Growler did not want to be left at the mercy of Lone Star, however, and used Riley's antics on the rooftops as an opportunity to make his escape. Summoning all of the strength his ork body could muster, he broke the zip tie that bound his wrists. Then, dragging Raven along behind him, he slunk inside of the warehouse, where he freed Raven from his bindings and resumed the search for the missing cargo.

Part Four: Tragedy Strikes
--------------------------
Back on the rooftops, Riley managed to stay one step ahead of Lone Star, but had not anticipated that spirits might be on her tail. As she lined up a shot on an officer on the ground, she got hit from behind by a blast of freezing cold air. She wasn't long in responding, however. She bolted back toward the building she came from, shot out a window on one of the lower floors, and jumped through it. Riley rolled her ankle in the process, but she didn't let that slow her down as she sped down the stairs toward the exit.

Back inside the warehouse, it dawned on Growler and Raven that they still needed to get out of the warehouse. The best that they could come up with was to try and hold their position until they had an opening to escape. Growler set up a couple of crates to use as support for his heavy machine gun, while Raven took position behind him. The first Lone Star to come into sight was treated to a barrage from Growler's machine gun, then finished off by a couple of controlled bursts from Raven's assault rifle. The pair was not prepared for the two officers that followed, and their concentrated fire was enough to bring Growler down.

[RIP Growler](//sranarchy.jamesbishop.ca/2017/03/growlers-eulogy).

By this time, Riley had returned to her Bulldog, and raced back to the warehouse entrance, crushing the two Lone Star officers between her van and the wall of the warehouse in the process. Riley then got out of the van, and rushed out to collect Raven and Growler, still unaware of what had transpired just moments before.

Bonus: The Lone Star Holding Cell
---------------------------------
While the rest of the team was getting shot at by Lone Star, EddY was bonding with a dwarf named Lenny in one of their holding cells. The pair whiled away the hours swapping tales, EddY spinning a yarn about the bond he shares with his origami and his spirits, while Lenny related that he felt a real connection with his cat, who incidentally had a synthcoke addiction. EddY soon became restless and opted to escape, attempting to enlist the help of two troll gang-bangers he was sharing his cell with. They decided that it would be better to play a game of stomp the elf, however, and combat ensued.

The trolls proved to be little trouble, as EddY managed to reach past the wards put in place by Lone Star and called out to his spirit allies. A wind spirit burst forth, knocking the gangers to the ground, engulfed one of the trolls, and began sucking the air out of the man's lungs. Meanwhile, an earth spirit emerged from the concrete floor and began bludgeoning the remaining troll with a slab of said concrete.

With the two trolls looked after, EddY called upon his allies' might to break free of his cell.

Part Five: Wrapping Up
----------------------
What followed was a whirlwind of events. Riley and Raven loaded Growler's body into a nearby delivery van and fled the scene. They took the body to a nearby wharf, where they paid Growler his last respects. Then Riley took anything of value that Growler was carrying, including his jacket, and gave him a sailor's burial. Just as they did so, Riley received a phone call from EddY, who had since gone back to his apartment. He explained that he tried calling Growler, but he wasn't getting an answer. After discussing what had happened, the two agreed to meet at the Tidbit.

Once they arrived at the Tidbit, Riley quickly got to work at drinking herself into a stupor. EddY arrived shortly after, and discussed getting in contact with Mr. Johsnon to finish the run. They were soon approached by one Ken Ripper, who had learned that Riley would be in charge of Growler's debt. After some back and forth between the group and Ken, EddY calls Mr. Johnson, who once again was trying to get some sleep. An exchange was set up at an abandoned office building in Redmond, and the team was on its way.

When they arrived in Redmond, the group found a trio of black sedans with tinted windows. EddY was the first to approach and, after a brief exchange, Ken soon followed with Mr. Johnson's cargo. After inspecting the packages, which turned out to contain large quantities of drugs, the deal was done. Mr. Johnson received two thirds of his product, while the team received two thirds of its payment. Riley paid of Growler's debt to Ken, and the runners all went their separate ways.

EddY took Growler's bike, which had been willed to him, into an alleyway and took some drugs that had been given to him by his mentor. This began his vision quest that left him wandering the city with no idea of where he was or where his bike was.

Riley and Raven went to visit Miss Grievous' chop shop, where they negotiated to trade the three stolen delivery vans for a troll-sized Eurocar Westwind. Miss Grievous wanted to keep the car for herself, but after some convincing from Raven, she agreed to let Riley have it. Riley then left with the car, while Raven got a cab home.

Finally, Ken returned to his home and went back into the operating room he had there. He cleaned this room methodically, then began work on his sister, who he kept frozen, to ensure that she would stay alive until he could find a way to bring her back. Someday.
